# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from base.base import BasePage
import allure
import pytest
# from PageObjects.CallCenter.callCenter_page import CallCenterPage
from business.commodity_list.commodity_list_bs import CommodityListBusiness as CLB
from time import sleep
from common.read_yaml import ReadYaml
data = ReadYaml("data.yml").get_yaml_data()#读取数据
from business.commodity_list.commodity_list_assert import CommodityListAssert as CLA
from pagelocators.commodity_locs.commodity_list_page import CommodityListPage as CLP
import time
# @pytest.mark.usefixtures("_driver")
# @pytest.mark.data_center
# @pytest.mark.skip
class TestDataCenter():

    #数据中心
    @pytest.mark.smoke
    # @pytest.mark.usefixtures("close")
    # @pytest.mark.usefixtures("delete_commodity")
    @allure.feature('测试商品模块')
    @allure.story('测试添加商品模块')
    @allure.step('步骤：测试正常添加商品功能')
    @allure.title("测试正常添加商品功能正常")
    @pytest.mark.run(order=1)
    def test_enter_search(self,AB,CLB,driver1):
        AB[1].add_commodity()
        CLB[1].enter_search(text='道长牌T恤')
        assert CLA(CLB[0]).add_commodity_assert(expect_text='道长牌T恤')



    # @allure.feature('测试商品模块')
    # @allure.story('测试商品列表模块')
    # @allure.step('步骤：测试商品名称搜索功能')
    # @allure.title("测试商品货号搜索功能搜索正常")
    # @pytest.mark.run(order=2)
    # def test_product_code(self, CLB, driver1):
    #     CLB[1].product_code()
    #     assert CLA(CLB[0]).product_code_assert(text='货号：6946605')
    #
    # @allure.feature('测试商品模块')
    # @allure.story('测试商品列表模块')
    # @allure.step('步骤：测试商品品牌搜索功能')
    # @allure.title("测试商品品牌搜索功能搜索正常")
    # @pytest.mark.run(order=3)
    # def test_commodity_brand(self,CLB,driver1):
    #     CLB[1].commodity_brand()
    #     assert CLA(CLB[0]).commodity_brand_assert(text='iPhone 8 Plus')

    # @allure.feature('测试商品模块')
    # @allure.story('测试商品列表模块')
    # @allure.step('步骤：测试商品上下架搜索功能')
    # @allure.title("测试商品上下架搜索功能搜索正常")
    # @pytest.mark.run(order=4)
    # def test_shelf_status(self,CLB,driver1):
    #     CLB[1].shelf_status()
    #     assert CLA(CLB[0]).down_shelf_assert(text='iPhone 8 Plus')



if __name__ == '__main__':
    pytest.main(['-q', r'D:\daima\pytest-web-ui-automation-huice\tests\test_commodity_list\test_add_cmmodity.py'])